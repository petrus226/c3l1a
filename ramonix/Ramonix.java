import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

//SERVIDOR ESPERA CONNEXIONS A LES QUE ATENDRÀ
class Ramonix {

    // PORT EN EL QUE ESPERA CONNEXIONS
    static final int PORT = 5000;

    public int getEnergia() {
        return energia;
    }

    public void setEnergia(int energia) {
        this.energia = energia;
    }

    public int energia = 500;
    String cadenaEnergia;

    public Ramonix() {
        try {
            // CREA SOCKET I ESPERA
            ServerSocket ssServidor = new ServerSocket(PORT);
            System.out.println("Benvinguts a Ramonix");

            while (energia > 0) {
                // MÈTODE accept() CREA UN NOU Socket PER A COMUNICAR-SE AMB EL CLIENT
                Socket sCliente = ssServidor.accept();
                // CREE ELS FLUXOS D'ENTRADA I EIXIDA PER AL SOCKET
                // ASSOCIE FLUX EIXIDA DE DADES AL SOCKET DEL CLIENT
                InputStream isEntrada = sCliente.getInputStream();
                // ASSOCIE FLUX DE DADES A UN ALTRE FLUX TIPUS DataInputStream
                DataInputStream disEntrada = new DataInputStream(isEntrada);
                // ASSOCIE FLUX EIXIDA DE DADES AL SOCKET DEL CLIENT
                OutputStream osEixida = sCliente.getOutputStream();
                // ASSOCIE FLUX DE DADES A UN ALTRE FLUX TIPUS DataOutputStream
                DataOutputStream dosEixida = new DataOutputStream(osEixida);

                // REP CADENA DEL CLIENT I ENVIE RESPOSTA AL CLIENT

                dosEixida.writeUTF(energia + "");
                cadenaEnergia = disEntrada.readUTF();
                this.energia -= Integer.parseInt(cadenaEnergia);
                dosEixida.writeUTF(this.energia + "");

                System.out.println("Energia de Ramonix: " + this.energia);

                // TANQUE CONNEXIONS
                System.out.println("comunique al client que tanque...");
                sCliente.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] arg) {
        new Ramonix();
    }
}