public class Hacker extends Thread {
    String nom;
    SalaReunions sala;
    int power;

    public Hacker(String nom, SalaReunions sala) {
        this.nom = nom;
        this.sala = sala;
        if(nom == "Neo"){
            this.power = 20;
        }
        else if(nom == "P4q1T0"){
            this.power = 10;
        }
        else if(nom == "PaU3T"){
            this.power = 10;
        }
        else if(nom == "Ab4$t0$"){
            this.power = -10;
        }
    }

    public void run() {
        synchronized (this.sala) {
            try{
                if(this.nom == "Neo"){
                    this.sala.notifyAll();
                }
                else{
                    System.out.println("espera");
                    this.sala.wait();
                }
            }
            catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            try {
                new Atac(nom, this.power);
            } catch (IllegalThreadStateException e) {
                e.getMessage();
            } 
        }
    }
    public void miAtaque() {
        new Atac(nom, 20);
        
    }
}
