import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class Atac {
    static final String HOST = "localhost";
    static final int PORT = 5000;

    public Atac(String nom, int e) {
        int energia = -1;
        String canenaEnergia;
        boolean ramonixAlive = true;
        do{
            try {
                // ES CREA EL SOCKET
                Socket sCliente = new Socket(HOST, PORT);
                // CREE ELS FLUXOS D'ENTRADA I EIXIDA PER AL SOCKET

                // ASSOCIE FLUX EIXIDA DE DADES AL SOCKET DEL CLIENT
                InputStream isEntrada = sCliente.getInputStream();
                // ASSOCIE FLUX DE DADES A UN ALTRE FLUX TIPUS DataInputStream
                DataInputStream disEntrada = new DataInputStream(isEntrada);

                // ASSOCIE FLUX EIXIDA DE DADES AL SOCKET DEL CLIENT
                OutputStream osEixida = sCliente.getOutputStream();
                // ASSOCIE FLUX DE DADES A UN ALTRE FLUX TIPUS DataOutputStream
                DataOutputStream dosEixida = new DataOutputStream(osEixida);


                if (energia == 0) {
                    System.out.println("Ramonix tango down");
                    dosEixida.writeUTF("Comunique al servidor que tanque");
                    sCliente.close();
                }

                // ENVIE CADENA I MOSTRE RESPOSTA DEL SERVIDOR

                canenaEnergia = disEntrada.readUTF();
                energia = Integer.parseInt(canenaEnergia);

                dosEixida.writeUTF(e + "");
                canenaEnergia = disEntrada.readUTF();

                System.out.println("*****" + nom + "ataca...");


                // TANQUE EL SOCKET
                sCliente.close();
                
            } catch (UnknownHostException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                ramonixAlive = false;
                System.out.println("Tancant connexió...");
                System.out.println(ex.getMessage());
            }
        }while(ramonixAlive);
    }

}
